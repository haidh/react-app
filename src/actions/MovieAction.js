import axios from "axios";
import {
  GET_ERRORS,
  GET_ALL_MOVIE,
  GET_MOVIE_PAGINATION,
  DELETE_MOVIE,
  DELETE_LIST_MOVIE,
  UPDATE_MOVIE,
  CREATE_MOVIE,
  GET_MOVIE_DETAIL,
} from "../action_types/types";
import { toast } from "react-toastify";
import API from '../api';
export const getAllMovie = () => async (dispatch) => {
  try {
    const res = await API.get("movie");
    dispatch({
      type: GET_ALL_MOVIE,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const getPagination = (search) => async (dispatch) => {
  try {
    search.page = search.page - 1;
    const url =
      "movie/pagination?page=" +
      search.page +
      "&size=" +
      search.size +
      "&sort=" +
      search.sortBy +
      "," +
      search.sort +
      "&name=" +
      search.name;
    const res = await API.get(url, search);
    dispatch({
      type: GET_MOVIE_PAGINATION,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const create = (movie, history) => async (dispatch) => {
  try {
    const res = await API.post(
      "movie/create",
      movie
    );
    dispatch({
      type: CREATE_MOVIE,
      payload: res.data,
    });
    toast.success("Create Movie Success", {});
    history.push("/movie");
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const update = (movie, history) => async (dispatch) => {
  try {
    const res = await API.post(
      "movie/update",
      movie
    );
    dispatch({
      type: UPDATE_MOVIE,
      payload: res.data,
    });
    toast.success("Update Movie Success", {});
    history.push("/movie");
  } catch (error) {
    toast.error(error.response.data.message);
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data,
    });
  }
};

export const deleteById = (id) => async (dispatch) => {
  try {
    const res = await API.delete(
      `movie/delete/${id}`
    );
    dispatch({
      type: DELETE_MOVIE,
      payload: res.data,
    });
    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const deleteList = (listId) => async (dispatch) => {
  try {
    const res = await API.delete(
      `movie/delete?listId=` + listId
    );

    dispatch({
      type: DELETE_LIST_MOVIE,
      payload: res.data,
    });

    toast.success(res.data);
  } catch (error) {
    toast.error(error.response.data.message);
  }
};

export const getMovieDetail = (id) => async (dispatch) => {
  try {
    const res = await API.get(`movie/${id}`);
    dispatch({
      type: GET_MOVIE_DETAIL,
      payload: res.data,
    });
  } catch (error) {
    toast.error(error.response.data.message);
  }
};
