import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { login } from "../../../actions/UserAction";
import {Link} from "react-router-dom";
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
    };
  }

  
  componentDidMount() {
    if (this.props.validToken) {
      this.props.history.push("/user");
      window.location.reload();
    } 
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.validToken) {
      this.props.history.push("/user");
      window.location.reload();
    } 
  }

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
    const LoginRequest = {
      username: this.state.username,
      password: this.state.password,
    };

    this.props.login(LoginRequest);
  };
  render() {
    return (
      <div>
        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />
          <div className="content-wrapper">
            <div className="content-header row"></div>
            <div className="content-body">
              <section className="row flexbox-container">
                <div className="col-12 d-flex align-items-center justify-content-center">
                  <div className="col-lg-4 col-md-8 col-10 box-shadow-2 p-0">
                    <div className="card border-grey border-lighten-3 px-1 py-1 m-0">
                      <div className="card-header border-0">
                        <div className="card-title text-center">
                          <img
                            src="app-assets/images/logo/stack-logo-dark.png"
                            alt="branding logo"
                          />
                        </div>
                        <h6 className="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                          <span>Easily Using</span>
                        </h6>
                      </div>
                      <div className="card-content">
                        <div className="text-center">
                          <Link
                            to="/"
                            className="btn btn-social-icon mr-1 mb-1 btn-outline-facebook"
                          >
                            <span className="fa fa-facebook" />
                          </Link>
                          <Link
                            to="/"
                            className="btn btn-social-icon mr-1 mb-1 btn-outline-twitter"
                          >
                            <span className="fa fa-twitter" />
                          </Link>
                          <Link
                            to="/"
                            className="btn btn-social-icon mr-1 mb-1 btn-outline-linkedin"
                          >
                            <span className="fa fa-linkedin font-medium-4" />
                          </Link>
                          <Link
                            to="/"
                            className="btn btn-social-icon mr-1 mb-1 btn-outline-github"
                          >
                            <span className="fa fa-github font-medium-4" />
                          </Link>
                        </div>
                        <p className="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1">
                          <span>OR Using Account Details</span>
                        </p>
                        <div className="card-body">
                          <form
                            className="form-horizontal"
                            action="index.html"
                            noValidate
                            onSubmit={this.onSubmit}
                          >
                            <fieldset className="form-group position-relative has-icon-left">
                              <input
                                type="text"
                                className="form-control"
                                id="username"
                                name="username"
                                placeholder="Your Username"
                                value={this.state.username}
                                onChange={this.onChange}
                                required
                              />
                              <div className="form-control-position">
                                <i className="feather icon-user" />
                              </div>
                            </fieldset>
                            <fieldset className="form-group position-relative has-icon-left">
                              <input
                                type="password"
                                className="form-control"
                                id="password"
                                name="password"
                                placeholder="Enter Password"
                                value={this.state.password}
                                onChange={this.onChange}
                                required
                              />
                              <div className="form-control-position">
                                <i className="fa fa-key" />
                              </div>
                            </fieldset>
                            <div className="form-group row">
                              <div className="col-sm-6 col-12 text-center text-sm-left pr-0">
                                <fieldset>
                                  <input
                                    type="checkbox"
                                    id="remember-me"
                                    className="chk-remember"
                                  />
                                  <label htmlFor="remember-me">
                                    &nbsp;
                                    Remember Me
                                  </label>
                                </fieldset>
                              </div>
                              <div className="col-sm-6 col-12 float-sm-left text-center text-sm-right">
                                <Link
                                  to="/forget"
                                  className="card-link"
                                >
                                  Forgot Password?
                                </Link>
                              </div>
                            </div>
                            <button
                              type="submit"
                              className="btn btn-outline-primary btn-block"
                            >
                              <i className="feather icon-unlock" /> Login
                            </button>
                          </form>
                        </div>
                        <p className="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1">
                          <span>New to Stack ?</span>
                        </p>
                        <div className="card-body">
                          <Link
                            to="/register"
                            className="btn btn-outline-danger btn-block"
                          >
                            <i className="feather icon-user" /> Register
                          </Link>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
        {/* END: Content*/}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user.data,
  errors: state.errors,
  validToken :state.user.validToken
});

Login.propTypes = {
  user: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  login: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, { login })(Login);
