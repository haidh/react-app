import React, { Component } from "react";
import "../../../style.css";
import Header from "../../layout/Header";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getSeasonByMovie, getAllSeason } from "../../../actions/SeasonAction";
import { getAllMovie } from "../../../actions/MovieAction";
import {
  getEpisodeByMovieSeason,
  getAllEpisode,
} from "../../../actions/EpisodeAction";
import { update, getDetail, saveFile } from "../../../actions/SentenceAction";
import { trackPromise } from "react-promise-tracker";
import { ToastContainer } from "react-toastify";
import classnames from "classnames";
import Select from "react-select";
class SentenceUpdate extends Component {
  state = {
    page: 1,
    size: 10,
    sentenceId: "",
    sentence: { value: "", isInvalid: false, message: "" },
    translation: { value: "", isInvalid: false, message: "" },
    audio: {},
    selectedFile: null,
    isInvalidForm: true,
    movies: [],
    movieSelect: [],
    movieIdSelected: "",
    movieSelectShow: "",
    seasons: [],
    seasonSelect: [],
    seasonSelectShow: "",
    seasonIdSelected: "",
    episodes: [],
    episodeSelect: [],
    episodeSelectShow: "",
    episodeIdSelected: "",
  };

  componentDidMount() {
    const query = new URLSearchParams(this.props.location.search);
    const page = parseInt(query.get("page"));
    const size = parseInt(query.get("size"));
    if (!isNaN(page) && !isNaN(size)) {
      this.setState(
        {
          page: page,
          size: size,
        }
      );
    }
    const id = this.props.match.params.id;

    trackPromise(this.props.getDetail(id)).then(() => {
      const sentenceDetail = this.props.sentenceDetail;

      const movie = {
        ...sentenceDetail.movie,
        value: sentenceDetail.movie.name,
        label: sentenceDetail.movie.name,
      };
      const season = {
        ...sentenceDetail.season,
        value: sentenceDetail.season.name,
        label: sentenceDetail.season.name,
      };
      const episode = {
        ...sentenceDetail.episode,
        value: sentenceDetail.episode.name,
        label: sentenceDetail.episode.name,
      };

      this.setState({
        sentenceId: sentenceDetail.id,
        sentence: { value: sentenceDetail.sentence },
        translation: { value: sentenceDetail.translation },
        seasonSelectShow: season,
        movieSelectShow: movie,
        episodeSelectShow: episode,
        movieIdSelected: movie.id,
        seasonIdSelected: season.id,
        episodeIdSelected: episode.id,
        audio: sentenceDetail.audio,
      });
      trackPromise(this.props.getAllMovie(this.state)).then(
        trackPromise(this.props.getAllSeason(this.state)).then(
          trackPromise(this.props.getAllEpisode(this.state))
        )
      );
    });
  }

  reload = () => {
    this.setState({
      sentenceId: "",
      sentence: { value: "", isInvalid: false, message: "" },
      translation: { value: "", isInvalid: false, message: "" },
      audio: {},
      selectedFile: null,
      isInvalidForm: true,
      movies: [],
      movieSelect: [],
      movieIdSelected: "",
      seasons: [],
      seasonSelect: [],
      seasonSelectShow: "",
      seasonIdSelected: "",
      episodes: [],
      episodeSelect: [],
      episodeSelectShow: "",
      episodeIdSelected: "",
    });
  };

  onChangeInput = (e) => {
    this.setState({
      [e.target.name]: {
        ...this.state[e.target.name],
        value: e.target.value,
      },
    });
  };

  update = (e) => {
    e.preventDefault();
    const query = new URLSearchParams(this.props.location.search);
    const page = parseInt(query.get("page"));
    const size = parseInt(query.get("size"));
    if (!isNaN(page) && !isNaN(size)) {
      this.setState(
        {
          page: page,
          size: size,
        },
        () => {
          this.props.update(this.state, this.props.history);
        }
      );
    }
  };

  selectFile = (e) => {
    this.setState(
      {
        selectedFile: e.target.files[0],
      },
      () => {
        trackPromise(this.props.saveFile(this.state));
      }
    );
  };
  render() {
    const {
      sentence,
      translation,
      movieSelect,
      seasonSelect,
      episodeSelect,
    } = this.state;
    const sentenceGroup = classnames("form-control border-primary", {
      "is-invalid": sentence.isInvalid,
    });

    return (
      <React.Fragment>
        <Header />
        <MenuSideBar />
        {/* Panel search */}

        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />
          <div className="content-wrapper">
            <div className="content-header row">
              <ToastContainer
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
              />
              <div className="content-header-left col-md-6 col-12 mb-2">
                <h3 className="content-header-title mb-0">Sentence</h3>
                <div className="row breadcrumbs-top">
                  <div className="breadcrumb-wrapper col-12">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/user">Home</Link>
                      </li>
                      <li className="breadcrumb-item">
                        <Link to="/sentence">Sentence</Link>
                      </li>
                      <li className="breadcrumb-item active">
                        Update Sentence
                      </li>
                    </ol>
                  </div>
                </div>
              </div>
            </div>
            <div className="content-body">
              <div className="row">
                <div className="col-md-12">
                  <div className="card">
                    <div className="card-header">
                      <h4
                        className="card-title"
                        id="horz-layout-colored-controls"
                      >
                        Update Sentence
                      </h4>
                      <a className="heading-elements-toggle">
                        <i className="fa fa-ellipsis-v font-medium-3" />
                      </a>
                      <div className="heading-elements">
                        <ul className="list-inline mb-0">
                          <li>
                            <a data-action="collapse">
                              <i className="feather icon-minus" />
                            </a>
                          </li>
                          <li>
                            <a data-action="reload" onClick={this.reload}>
                              <i className="feather icon-rotate-cw" />
                            </a>
                          </li>
                          <li>
                            <a data-action="expand">
                              <i className="feather icon-maximize" />
                            </a>
                          </li>
                          <li>
                            <a data-action="close">
                              <i className="feather icon-x" />
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="card-content collpase show">
                      <div className="card-body">
                        <form
                          className="form form-horizontal"
                          onSubmit={this.update}
                        >
                          <div className="form-body">
                            <div className="row">
                              <div className="col-md-12">
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Movie
                                  </label>
                                  <div className="col-md-8">
                                    <Select
                                      options={movieSelect}
                                      className="border-primary"
                                      onChange={this.onChangeSelectMovie}
                                      value={this.state.movieSelectShow}
                                      isDisabled
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="col-md-12">
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Season
                                  </label>
                                  <div className="col-md-8">
                                    <Select
                                      options={seasonSelect}
                                      className="border-primary"
                                      onChange={this.onChangeSelectSeason}
                                      value={this.state.seasonSelectShow}
                                      isDisabled
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="col-md-12">
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Episode
                                  </label>
                                  <div className="col-md-8">
                                    <Select
                                      options={episodeSelect}
                                      className="border-primary"
                                      onChange={this.onChangeSelectEpisode}
                                      value={this.state.episodeSelectShow}
                                      isDisabled
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="col-md-12">
                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Sentence
                                    <span style={{ color: "red" }}>
                                      &nbsp; *
                                    </span>
                                  </label>
                                  <div className="col-md-8 ">
                                    <textarea
                                      id="sentenceId"
                                      rows="4"
                                      className="form-control border-primary"
                                      name="sentence"
                                      placeholder="Sentence"
                                      value={sentence.value}
                                      onChange={(e) => this.onChangeInput(e)}
                                      maxLength="1000"
                                    ></textarea>

                                    {sentence.isInvalid && (
                                      <div className="help-block">
                                        <ul role="alert">
                                          <li>{sentence.message}</li>
                                        </ul>
                                      </div>
                                    )}
                                  </div>
                                </div>

                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Translation
                                  </label>
                                  <div className="col-md-8">
                                    <textarea
                                      id="translationId"
                                      rows="4"
                                      className="form-control border-primary"
                                      name="translation"
                                      placeholder="Translation"
                                      value={translation.value}
                                      onChange={(e) => this.onChangeInput(e)}
                                      maxLength="1000"
                                    ></textarea>
                                  </div>
                                </div>

                                <div className="form-group row">
                                  <label className="col-md-4 control-label">
                                    Audio
                                  </label>
                                  <div className="col-md-8">
                                    <div>
                                      <div>
                                        <input
                                          className="btn btn-primary mb-1"
                                          type="file"
                                          file-input="files"
                                          onChange={this.selectFile}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="form-actions center">
                            <button
                              type="submit"
                              className="btn btn-primary mr-1"
                            >
                              <i className="fa fa-check-square-o" /> Save
                            </button>
                            <Link
                              type="button"
                              className="btn btn-warning mr-1"
                              to={
                                `/sentence?page=` +
                                this.state.page +
                                `&size=` +
                                this.state.size
                              }
                            >
                              <i className="feather icon-x" /> Cancel
                            </Link>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Footer />
        {/* END: Content*/}
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => ({
  movies: state.movies.allMovie,
  seasons: state.seasons.seasonByMovie,
  episodes: state.episodes.episodeByMovieSeason,
  sentenceDetail: state.sentences.sentenceDetail,
});

export default connect(mapStateToProps, {
  getAllMovie,
  getSeasonByMovie,
  getAllSeason,
  getEpisodeByMovieSeason,
  getAllEpisode,
  update,
  getDetail,
  saveFile,
})(SentenceUpdate);
