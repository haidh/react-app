import React, { Component } from "react";
import "../../../style.css";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  getRole,
  deleteRole,
  deleteListRole,
} from "../../../actions/RoleAction";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { TextField, Checkbox } from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";
import { Link } from "react-router-dom";
import { trackPromise } from "react-promise-tracker";
import Header from "../../layout/Header";
import { Modal } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";

class Role extends Component {
  state = {
    name: "",
    description: "",
    page: 1,
    size: 10,
    sortBy: "id",
    sort: "ASC",
    sorted: false,
    roles: [],
    totalElements: 0,
    totalPages: 0,
    roleIdDelete: "",
    listRoleIdDelete: [],
    showModal: false,
    showModalMany: false,
    checkAll: false,
  };

  componentDidMount() {
    trackPromise(this.props.getRole(this.state));
  }
  componentWillReceiveProps(nextProps) {
    const roles = nextProps.roles;
    if (roles.length > 0) {
      roles.map((role) => {
        role.isChecked = false;
      });
    }
    this.setState({
      page: nextProps.page + 1,
      roles: roles,
      totalElements: nextProps.totalElements,
      totalPages: nextProps.totalPages,
    });
    return this.state;
  }

  onChangeInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  search = () => {
    //  Load spinner when call action search
    trackPromise(this.props.getRole(this.state));
  };

  onChangeSizePage = (e) => {
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        trackPromise(this.props.getRole(this.state));
      }
    );
  };

  onChangePage = (e, page) => {
    this.setState(
      {
        page: page,
      },
      () => {
        trackPromise(this.props.getRole(this.state));
      }
    );
  };
  onChangeCheckboxAll = () => {
    this.state.checkAll = !this.state.checkAll;

    const listRoleIdDelete = [];
    //  SET STATTUS CHECKED TO CHILDREN
    this.state.roles.map((value) => {
      value.isChecked = this.state.checkAll;
      if (this.state.checkAll) {
        listRoleIdDelete.push(value.id);
      }
    });

    //  SET STATUS CHECK ALL
    this.setState({
      checkAll: this.state.checkAll,
      listRoleIdDelete: listRoleIdDelete,
    });
  };

  onChangeCheckbox = (value) => {
    value.isChecked = !value.isChecked;
    let roles = this.state.roles;
    let countChecked = 0;
    const listRoleIdDelete = [];
    roles.map((role) => {
      if (role.name === value.name) {
        role.isChecked = value.isChecked;
      }

      if (role.isChecked === true) {
        countChecked++;
        listRoleIdDelete.push(role.id);
      }
    });
    if (countChecked === roles.length) {
      this.setState({
        checkAll: true,
      });
    } else {
      this.setState({
        checkAll: false,
      });
    }
    this.setState({
      roles: roles,
      listRoleIdDelete: listRoleIdDelete,
    });
  };

  reloadPage = () => {
    this.setState(
      {
        name: "",
        description: "",
        page: 1,
        size: 10,
        sortBy: "id",
        sort: "ASC",
        sorted: false,
        roles: [],
        totalElements: 0,
        totalPages: 0,
        roleIdDelete: "",
        listRoleIdDelete: [],
        showModal: false,
        showModalMany: false,
        checkAll: false,
      },
      () => {
        trackPromise(this.props.getRole(this.state));
      }
    );
  };

  deleteModal = (id) => {
    this.setState({
      showModal: true,
      roleIdDelete: id,
    });
  };

  handleCloseModal = () => this.setState({ showModal: false });

  deleteRole = () => {
    const id = this.state.roleIdDelete;

    this.props.deleteRole(id).then(() => {
      this.handleCloseModal();
      trackPromise(this.props.getRole(this.state)).then(() => {});
    });
  };

  deleteManyModal = () => {
    if (this.state.listRoleIdDelete.length > 0) {
      this.setState({
        showModalMany: true,
      });
    } else {
      toast.warning("Please Select One Role");
    }
  };

  handleCloseModalMany = () => this.setState({ showModalMany: false });

  deleteManyRole = () => {
    const listRoleIdDelete = this.state.listRoleIdDelete;
    this.props.deleteListRole(listRoleIdDelete).then(() => {
      this.handleCloseModalMany();
      trackPromise(this.props.getRole(this.state));
    });
  };

  sortBy = (fieldName) => {
    let sorted = this.state.sorted;
    sorted = !sorted;
    if (this.state.sorted) {
      this.setState({
        sort: "ASC",
      });
    } else {
      this.setState({
        sort: "DESC",
      });
    }
    this.setState(
      {
        sortBy: fieldName,
        sorted: sorted,
      },
      () => {
        console.log(this.state);
        trackPromise(this.props.getRole(this.state));
      }
    );
  };

  render() {
    return (
      <React.Fragment>
        <ToastContainer
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <Modal
          name="delete"
          show={this.state.showModal}
          animation={false}
          onHide={this.handleCloseModal}
        >
          <Modal.Header closeButton className="bg-warning white">
            <Modal.Title>MODAL DELETE</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you want to delete this role?</Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={this.deleteRole}
            >
              OK
            </button>
            <button
              type="button"
              className="btn grey btn-outline-secondary "
              onClick={this.handleCloseModal}
            >
              Cancel
            </button>
          </Modal.Footer>
        </Modal>

        <Modal
          name="deleteAll"
          show={this.state.showModalMany}
          animation={false}
          onHide={this.handleCloseModalMany}
        >
          <Modal.Header closeButton className="bg-warning white">
            <Modal.Title>MODAL DELETE</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you want to delete these roles?</Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={this.deleteManyRole}
            >
              OK
            </button>
            <button
              type="button"
              className="btn grey btn-outline-secondary "
              onClick={this.handleCloseModalMany}
            >
              Cancel
            </button>
          </Modal.Footer>
        </Modal>

        <Header />
        <MenuSideBar />
        {/* Panel search */}

        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />

          <div className="content-wrapper">
            <div className="content-header row">
              <div className="content-header-left col-md-6 col-12 mb-2">
                <h3 className="content-header-title mb-0">Role</h3>
                <div className="row breadcrumbs-top">
                  <div className="breadcrumb-wrapper col-12">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/user">Home</Link>
                      </li>
                      <li className="breadcrumb-item active">Role</li>
                    </ol>
                  </div>
                </div>
              </div>
              <div className="content-header-right text-md-right col-md-6 col-12">
                <div className="form-group">
                  <Link
                    className="btn-icon btn fonticon-container"
                    type="button"
                    to="/role/create"
                  >
                    <i className="fa fa-plus"></i>
                    <label className="fonticon-classname">Add Role</label>
                  </Link>
                  <button
                    className="btn-icon btn fonticon-container"
                    type="button"
                    onClick={() => this.deleteManyModal()}
                  >
                    <i className="fa fa-times"></i>
                    <label className="fonticon-classname">Delete</label>
                  </button>
                  <button
                    className="btn-icon btn fonticon-container"
                    type="button"
                    onClick={this.reloadPage}
                  >
                    <i className="fa fa-refresh"></i>
                    <label className="fonticon-classname">Refresh</label>
                  </button>
                </div>
              </div>
            </div>
            <div className="content-body">
              {/* Basic Elements start */}
              <section className="basic-elements">
                <div className="row">
                  <div className="col-md-12">
                    <div className="card">
                      <div className="card-content">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <TextField
                                id="roleId"
                                label="Role Name"
                                className="form-control"
                                color="secondary"
                                value={this.state.name}
                                name="name"
                                onChange={(e) => this.onChangeInput(e)}
                              />
                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <TextField
                                id="deascriptionId"
                                label="Role Description"
                                className="form-control"
                                color="secondary"
                                name="description"
                                value={this.state.description}
                                onChange={(e) => this.onChangeInput(e)}
                              />
                            </div>
                            <div className="col-xl-1 col-lg-1 col-md-1 mb-1">
                              <a
                                href="#"
                                className="btn btn-social-icon btn-outline-twitter btn-outline-cyan"
                                onClick={this.search}
                              >
                                <span className="fa fa-search"></span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              {/* Basic Inputs end */}

              <section id="decimal">
                <div className="row">
                  <div className="col-12">
                    <div className="card">
                      <div className="card-header">
                        <a href="/#" className="heading-elements-toggle">
                          <i className="fa fa-ellipsis-v font-medium-3" />
                        </a>
                        <div className="heading-elements">
                          <ul className="list-inline mb-0">
                            <li>
                              <a data-action="collapse">
                                <i className="feather icon-minus" />
                              </a>
                            </li>
                            <li>
                              <a data-action="reload" onClick={this.reloadPage}>
                                <i className="feather icon-rotate-cw" />
                              </a>
                            </li>
                            <li>
                              <a data-action="expand">
                                <i className="feather icon-maximize" />
                              </a>
                            </li>
                            <li>
                              <a data-action="close">
                                <i className="feather icon-x" />
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="card-content collapse show">
                        <div className="card-body card-dashboard">
                          <table className="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th style={{ width: "5%" }}>
                                  <Checkbox
                                    checked={this.state.checkAll}
                                    onChange={this.onChangeCheckboxAll}
                                    name="check"
                                    color="primary"
                                    value="checkAll"
                                  />
                                </th>
                                <th style={{ width: "5%" }}>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("id")}
                                  >
                                    ID
                                  </a>
                                </th>
                                <th>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("name")}
                                  >
                                    Role Name
                                  </a>
                                </th>
                                <th>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("description")}
                                  >
                                    Role Description
                                  </a>
                                </th>
                                <th style={{ width: "15%" }}>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.roles.map((value, index) => (
                                <tr key={index}>
                                  <td>
                                    <Checkbox
                                      checked={value.isChecked}
                                      color="primary"
                                      onChange={() =>
                                        this.onChangeCheckbox(value)
                                      }
                                    />
                                  </td>
                                  <td className="styleTD">
                                    {index +
                                      (this.state.page - 1) * this.state.size +
                                      1}
                                  </td>
                                  <td className="styleTD">{value.name}</td>
                                  <td className="styleTD">
                                    {value.description}
                                  </td>
                                  <td>
                                    <Link
                                      className="btn-icon btn warning editButton"
                                      type="button"
                                      to={`/role/view/${value.id}`}
                                    >
                                      <i className="fa fa-eye"></i>
                                    </Link>
                                    <Link
                                      className="btn-icon btn primary editButton"
                                      type="button"
                                      to={`/role/update/${value.id}`}
                                    >
                                      <i className="fa fa-pencil-square"></i>
                                    </Link>
                                    <button
                                      className="btn-icon btn danger deleteButton"
                                      type="button"
                                      onClick={() => this.deleteModal(value.id)}
                                    >
                                      <i className="fa fa-trash"></i>
                                    </button>
                                  </td>
                                </tr>
                              ))}
                            </tbody>
                          </table>

                          <div className="row">
                            <div className="offset-5 col-2 text-right">
                              <label className="control-label labelPagination">
                                Show
                              </label>
                            </div>
                            <div className="col-1">
                              <select
                                name="size"
                                className="custom-select custom-select-sm form-control form-control-sm"
                                value={this.state.size}
                                onChange={this.onChangeSizePage}
                              >
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                              </select>
                            </div>
                            <div className="col-4">
                              <Pagination
                                name="page"
                                count={this.state.totalPages}
                                color="primary"
                                showFirstButton
                                showLastButton
                                onChange={this.onChangePage}
                                page={this.state.page}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
        {/* Modal */}

        <Footer />
        {/* END: Content*/}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  roles: state.roles.roles,
  totalPages: state.roles.totalPages,
  totalElements: state.roles.totalElements,
  page: state.roles.page,
  deleteSuccess: state.roles.deleteSuccess,
});

Role.propTypes = {
  roles: PropTypes.array.isRequired,
  totalPages: PropTypes.number.isRequired,
  totalElements: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  deleteSuccess: PropTypes.string.isRequired,
  getRole: PropTypes.func.isRequired,
  deleteRole: PropTypes.func.isRequired,
  deleteListRole: PropTypes.func.isRequired,
};
export default connect(mapStateToProps, {
  getRole,
  deleteRole,
  deleteListRole,
})(Role);
