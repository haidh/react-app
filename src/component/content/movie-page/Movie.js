import React, { Component } from "react";
import "../../../style.css";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  getPagination,
  deleteById,
  deleteList,
} from "../../../actions/MovieAction";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { TextField, Checkbox } from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";
import { Link } from "react-router-dom";
import { trackPromise } from "react-promise-tracker";
import Header from "../../layout/Header";
import { Modal } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";

class Movie extends Component {
  state = {
    name: "",
    description: "",
    page: 1,
    size: 10,
    sortBy: "id",
    sort: "ASC",
    sorted: false,
    movies: [],
    totalElements: 0,
    totalPages: 0,
    idDelete: "",
    listIdDelete: [],
    showModal: false,
    showModalMany: false,
    checkAll: false,
  };

  componentDidMount() {
    trackPromise(this.props.getPagination(this.state));
  }
  componentWillReceiveProps(nextProps) {
    const movies = nextProps.movies;
    if (movies.length > 0) {
      movies.map((movie) => {
        movie.isChecked = false;
      });
    }
    this.setState({
      page: nextProps.page + 1,
      movies: movies,
      totalElements: nextProps.totalElements,
      totalPages: nextProps.totalPages,
    });
    return this.state;
  }

  onChangeInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  search = () => {
    //  Load spinner when call action search
    trackPromise(this.props.getPagination(this.state));
  };

  onChangeSizePage = (e) => {
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        trackPromise(this.props.getPagination(this.state));
      }
    );
  };

  onChangePage = (e, page) => {
    this.setState(
      {
        page: page,
      },
      () => {
        trackPromise(this.props.getPagination(this.state));
      }
    );
  };
  onChangeCheckboxAll = () => {
    this.state.checkAll = !this.state.checkAll;

    const listIdDelete = [];
    //  SET STATTUS CHECKED TO CHILDREN
    this.state.movies.map((value) => {
      value.isChecked = this.state.checkAll;
      if (this.state.checkAll) {
        listIdDelete.push(value.id);
      }
    });

    //  SET STATUS CHECK ALL
    this.setState({
      checkAll: this.state.checkAll,
      listIdDelete: listIdDelete,
    });
  };

  onChangeCheckbox = (value) => {
    value.isChecked = !value.isChecked;
    let { movies } = this.state;
    let countChecked = 0;
    const listIdDelete = [];
    movies.map((movie) => {
      if (movie.name === value.name) {
        movie.isChecked = value.isChecked;
      }

      if (movie.isChecked === true) {
        countChecked++;
        listIdDelete.push(movie.id);
      }
    });
    if (countChecked === movies.length) {
      this.setState({
        checkAll: true,
      });
    } else {
      this.setState({
        checkAll: false,
      });
    }
    this.setState({
      movies: movies,
      listIdDelete: listIdDelete,
    });
  };

  reloadPage = () => {
    this.setState(
      {
        name: "",
        description: "",
        page: 1,
        size: 10,
        sortBy: "id",
        sort: "ASC",
        sorted: false,
        movies: [],
        totalElements: 0,
        totalPages: 0,
        idDelete: "",
        listIdDelete: [],
        showModal: false,
        showModalMany: false,
        checkAll: false,
      },
      () => {
        trackPromise(this.props.getPagination(this.state));
      }
    );
  };

  deleteModal = (id) => {
    this.setState({
      showModal: true,
      idDelete: id,
    });
  };

  handleCloseModal = () => this.setState({ showModal: false });

  deleteMovie = () => {
    const id = this.state.idDelete;

    this.props.deleteById(id).then(() => {
      this.handleCloseModal();
      trackPromise(this.props.getPagination(this.state)).then(() => {});
    });
  };

  deleteManyModal = () => {
    if (this.state.listIdDelete.length > 0) {
      this.setState({
        showModalMany: true,
      });
    } else {
      toast.warning("Please Select One Movie");
    }
  };

  handleCloseModalMany = () => this.setState({ showModalMany: false });

  deleteManyMovie = () => {
    const listIdDelete = this.state.listIdDelete;
    this.props.deleteList(listIdDelete).then(() => {
      this.handleCloseModalMany();
      trackPromise(this.props.getPagination(this.state));
    });
  };

  sortBy = (fieldName) => {
    let sorted = this.state.sorted;
    sorted = !sorted;
    if (this.state.sorted) {
      this.setState({
        sort: "ASC",
      });
    } else {
      this.setState({
        sort: "DESC",
      });
    }
    this.setState(
      {
        sortBy: fieldName,
        sorted: sorted,
      },
      () => {
        trackPromise(this.props.getPagination(this.state));
      }
    );
  };

  render() {
    return (
      <React.Fragment>
        <ToastContainer
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
        <Modal
          name="delete"
          show={this.state.showModal}
          animation={false}
          onHide={this.handleCloseModal}
        >
          <Modal.Header closeButton className="bg-warning white">
            <Modal.Title>MODAL DELETE</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you want to delete this movie?</Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={this.deleteMovie}
            >
              OK
            </button>
            <button
              type="button"
              className="btn grey btn-outline-secondary "
              onClick={this.handleCloseModal}
            >
              Cancel
            </button>
          </Modal.Footer>
        </Modal>

        <Modal
          name="deleteAll"
          show={this.state.showModalMany}
          animation={false}
          onHide={this.handleCloseModalMany}
        >
          <Modal.Header closeButton className="bg-warning white">
            <Modal.Title>MODAL DELETE</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you want to delete these movies?</Modal.Body>
          <Modal.Footer>
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={this.deleteManyMovie}
            >
              OK
            </button>
            <button
              type="button"
              className="btn grey btn-outline-secondary "
              onClick={this.handleCloseModalMany}
            >
              Cancel
            </button>
          </Modal.Footer>
        </Modal>

        <Header />
        <MenuSideBar />
        {/* Panel search */}

        {/* BEGIN: Content*/}
        <div className="app-content content">
          <div className="content-overlay" />

          <div className="content-wrapper">
            <div className="content-header row">
              <div className="content-header-left col-md-6 col-12 mb-2">
                <h3 className="content-header-title mb-0">Movie</h3>
                <div className="row breadcrumbs-top">
                  <div className="breadcrumb-wrapper col-12">
                    <ol className="breadcrumb">
                      <li className="breadcrumb-item">
                        <Link to="/user">Home</Link>
                      </li>
                      <li className="breadcrumb-item active">Movie</li>
                    </ol>
                  </div>
                </div>
              </div>
              <div className="content-header-right text-md-right col-md-6 col-12">
                <div className="form-group">
                  <Link
                    className="btn-icon btn fonticon-container"
                    type="button"
                    to="/movie/create"
                  >
                    <i className="fa fa-plus"></i>
                    <label className="fonticon-classname">Add Movie</label>
                  </Link>
                  <button
                    className="btn-icon btn fonticon-container"
                    type="button"
                    onClick={() => this.deleteManyModal()}
                  >
                    <i className="fa fa-times"></i>
                    <label className="fonticon-classname">Delete</label>
                  </button>
                  <button
                    className="btn-icon btn fonticon-container"
                    type="button"
                    onClick={this.reloadPage}
                  >
                    <i className="fa fa-refresh"></i>
                    <label className="fonticon-classname">Refresh</label>
                  </button>
                </div>
              </div>
            </div>
            <div className="content-body">
              {/* Basic Elements start */}
              <section className="basic-elements">
                <div className="row">
                  <div className="col-md-12">
                    <div className="card">
                      <div className="card-content">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <TextField
                                id="movieId"
                                label="Movie Name"
                                className="form-control"
                                color="secondary"
                                value={this.state.name}
                                name="name"
                                onChange={(e) => this.onChangeInput(e)}
                              />
                            </div>
                            <div className="col-xl-4 col-lg-6 col-md-12 mb-1">
                              <TextField
                                id="deascriptionId"
                                label="Movie Description"
                                className="form-control"
                                color="secondary"
                                name="description"
                                value={this.state.description}
                                onChange={(e) => this.onChangeInput(e)}
                              />
                            </div>
                            <div className="col-xl-1 col-lg-1 col-md-1 mb-1">
                              <a
                                className="btn btn-social-icon btn-outline-twitter btn-outline-cyan"
                                onClick={this.search}
                              >
                                <span className="fa fa-search"></span>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              {/* Basic Inputs end */}

              <section id="decimal">
                <div className="row">
                  <div className="col-12">
                    <div className="card">
                      <div className="card-header">
                        <a href="/#" className="heading-elements-toggle">
                          <i className="fa fa-ellipsis-v font-medium-3" />
                        </a>
                        <div className="heading-elements">
                          <ul className="list-inline mb-0">
                            <li>
                              <a data-action="collapse">
                                <i className="feather icon-minus" />
                              </a>
                            </li>
                            <li>
                              <a data-action="reload" onClick={this.reloadPage}>
                                <i className="feather icon-rotate-cw" />
                              </a>
                            </li>
                            <li>
                              <a data-action="expand">
                                <i className="feather icon-maximize" />
                              </a>
                            </li>
                            <li>
                              <a data-action="close">
                                <i className="feather icon-x" />
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div className="card-content collapse show">
                        <div className="card-body card-dashboard">
                          <table className="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th style={{ width: "5%" }}>
                                  <Checkbox
                                    checked={this.state.checkAll}
                                    onChange={this.onChangeCheckboxAll}
                                    name="check"
                                    color="primary"
                                    value="checkAll"
                                  />
                                </th>
                                <th style={{ width: "5%" }}>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("id")}
                                  >
                                    ID
                                  </a>
                                </th>
                                <th>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("name")}
                                  >
                                    Movie Name
                                  </a>
                                </th>
                                <th>
                                  <a
                                    className="tableHeaderLink"
                                    onClick={() => this.sortBy("description")}
                                  >
                                    Movie Description
                                  </a>
                                </th>
                                <th style={{ width: "15%" }}>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.movies.map((value, index) => (
                                <tr key={index}>
                                  <td>
                                    <Checkbox
                                      checked={value.isChecked}
                                      color="primary"
                                      onChange={() =>
                                        this.onChangeCheckbox(value)
                                      }
                                    />
                                  </td>
                                  <td className="styleTD">
                                    {index +
                                      (this.state.page - 1) * this.state.size +
                                      1}
                                  </td>
                                  <td className="styleTD">{value.name}</td>
                                  <td className="styleTD">
                                    {value.description}
                                  </td>
                                  <td>
                                    <Link
                                      className="btn-icon btn primary editButton"
                                      type="button"
                                      to={`/movie/update/${value.id}`}
                                    >
                                      <i className="fa fa-pencil-square"></i>
                                    </Link>
                                    <button
                                      className="btn-icon btn danger deleteButton"
                                      type="button"
                                      onClick={() => this.deleteModal(value.id)}
                                    >
                                      <i className="fa fa-trash"></i>
                                    </button>
                                  </td>
                                </tr>
                              ))}
                            </tbody>
                          </table>

                          <div className="row">
                            <div className="offset-5 col-2 text-right">
                              <label className="control-label labelPagination">
                                Show
                              </label>
                            </div>
                            <div className="col-1">
                              <select
                                name="size"
                                className="custom-select custom-select-sm form-control form-control-sm"
                                value={this.state.size}
                                onChange={this.onChangeSizePage}
                              >
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="30">30</option>
                              </select>
                            </div>
                            <div className="col-4">
                              <Pagination
                                name="page"
                                count={this.state.totalPages}
                                color="primary"
                                showFirstButton
                                showLastButton
                                onChange={this.onChangePage}
                                page={this.state.page}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
        {/* Modal */}

        <Footer />
        {/* END: Content*/}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  movies: state.movies.movies,
  totalPages: state.movies.totalPages,
  totalElements: state.movies.totalElements,
  page: state.movies.page,
  deleteSuccess: state.movies.deleteSuccess,
});

export default connect(mapStateToProps, {
  getPagination,
  deleteById,
  deleteList,
})(Movie);
