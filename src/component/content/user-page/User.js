import React, { Component } from "react";
import "../../../style.css";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  getUser,
  deleteUser,
  deleteListUser,
} from "../../../actions/UserAction";
import MenuSideBar from "../../layout/Menu";
import Footer from "../../layout/Footer";
import { TextField, Checkbox } from "@material-ui/core";
import Pagination from "@material-ui/lab/Pagination";
import { Link, Redirect } from "react-router-dom";
import { trackPromise } from "react-promise-tracker";
import Header from "../../layout/Header";
import { Modal } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";

class User extends Component {
  state = {
    username: "",
    fullName: "",
    email: "",
    page: 1,
    size: 10,
    sortBy: "id",
    sort: "ASC",
    sorted: false,
    users: [],
    totalElements: 0,
    totalPages: 0,
    userIsDelete: "",
    listUserIdDelete: [],
    showModal: false,
    showModalMany: false,
    checkAll: false,
    redirect: false,
  };
  componentDidMount = () => {
    trackPromise(
      this.props.getUser(this.state).then(() => {
        if (
          Object.keys(this.props.errors).length > 0 &&
          this.props.errors.status === 403
        ) {
          this.setState({ redirect: true });
        } else {
          this.setState({ redirect: false });
        }
      })
    );
  };
  componentWillReceiveProps = (nextProps) => {
    const users = nextProps.users;
    if (users.length > 0) {
      users.map((user) => {
        user.isChecked = false;
      });
    }
    this.setState({
      page: nextProps.page + 1,
      users: users,
      totalElements: nextProps.totalElements,
      totalPages: nextProps.totalPages,
    });
  };

  onChangeInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  search = () => {
    //  Load spinner when call action search
    trackPromise(this.props.getUser(this.state));
  };

  onChangeSizePage = (e) => {
    this.setState(
      {
        [e.target.name]: e.target.value,
      },
      () => {
        trackPromise(this.props.getRole(this.state));
      }
    );
  };

  onChangePage = (e, page) => {
    this.setState(
      {
        page: page,
      },
      () => {
        trackPromise(this.props.getUser(this.state));
      }
    );
  };
  onChangeCheckboxAll = () => {
    console.log("object")
    this.state.checkAll = !this.state.checkAll;

    const listUserIdDelete = [];
    //  SET STATTUS CHECKED TO CHILDREN
    this.state.users.map((value) => {
      value.isChecked = this.state.checkAll;
      if (this.state.checkAll) {
        listUserIdDelete.push(value.id);
      }
    });

    //  SET STATUS CHECK ALL
    this.setState({
      checkAll: this.state.checkAll,
      listUserIdDelete: listUserIdDelete,
    });
  };

  onChangeCheckbox = (value) => {
    value.isChecked = !value.isChecked;
    let users = this.state.users;
    let countChecked = 0;
    const listUserIdDelete = [];
    users.map((user) => {
      if (user.id === value.id) {
        user.isChecked = value.isChecked;
      }

      if (user.isChecked) {
        countChecked++;
        listUserIdDelete.push(user.id);
      }
    });
    if (countChecked == users.length) {
      this.setState({
        checkAll: true,
      });
    } else {
      this.setState({
        checkAll: false,
      });
    }
    this.setState({
      users: users,
      listUserIdDelete: listUserIdDelete,
    });
  };

  reloadPage = () => {
    this.setState(
      {
        username: "",
        fullName: "",
        email: "",
        page: 1,
        size: 10,
        sortBy: "id",
        sort: "ASC",
        sorted: false,
        users: [],
        totalElements: 0,
        totalPages: 0,
        userIsDelete: "",
        listUserIdDelete: [],
        showModal: false,
        showModalMany: false,
        checkAll: false,
        redirect: false,
      },
      () => {
        trackPromise(this.props.getUser(this.state));
      }
    );
  };

  deleteModal = (id) => {
    this.setState({
      showModal: true,
      userIdDelete: id,
    });
  };

  handleCloseModal = () => this.setState({ showModal: false });

  deleteUser = () => {
    const id = this.state.userIdDelete;
    this.props.deleteUser(id).then(() => {
      this.handleCloseModal();
      trackPromise(this.props.getUser(this.state));
    });
  };

  deleteManyModal = () => {
    if (this.state.listUserIdDelete.length > 0) {
      this.setState({
        showModalMany: true,
      });
    } else {
      toast.warning("Please Select One User");
    }
  };

  handleCloseModalMany = () => this.setState({ showModalMany: false });

  deleteManyRole = () => {
    const listUserIdDelete = this.state.listUserIdDelete;
    this.props.deleteListUser(listUserIdDelete).then(() => {
      this.handleCloseModalMany();
      trackPromise(this.props.getUser(this.state));
    });
  };

  sortBy = (fieldName) => {
    let sorted = this.state.sorted;
    sorted = !sorted;
    if (this.state.sorted) {
      this.setState({
        sort: "ASC",
      });
    } else {
      this.setState({
        sort: "DESC",
      });
    }
    this.setState(
      {
        sortBy: fieldName,
        sorted: sorted,
      },
      () => {
        trackPromise(this.props.getUser(this.state));
      }
    );
  };

  editUser = (id) => {
    console.log("edit");
    window.location.href(`/user/update/${id}`);
  };
  render() {
    const { redirect } = this.state;

    if (redirect) {
      return <Redirect to="/page-403" />;
    } else {
      return (
        <React.Fragment>
          <ToastContainer
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
          <Modal
            name="delete"
            show={this.state.showModal}
            animation={false}
            onHide={this.handleCloseModal}
          >
            <Modal.Header closeButton className="bg-warning white">
              <Modal.Title>MODAL DELETE</Modal.Title>
            </Modal.Header>
            <Modal.Body>Do you want to delete this user?</Modal.Body>
            <Modal.Footer>
              <button
                type="button"
                className="btn btn-outline-danger"
                onClick={this.deleteUser}
              >
                OK
              </button>
              <button
                type="button"
                className="btn grey btn-outline-secondary "
                onClick={this.handleCloseModal}
              >
                Cancel
              </button>
            </Modal.Footer>
          </Modal>

          <Modal
            name="deleteAll"
            show={this.state.showModalMany}
            animation={false}
            onHide={this.handleCloseModalMany}
          >
            <Modal.Header closeButton className="bg-warning white">
              <Modal.Title>MODAL DELETE</Modal.Title>
            </Modal.Header>
            <Modal.Body>Do you want to delete these users?</Modal.Body>
            <Modal.Footer>
              <button
                type="button"
                className="btn btn-outline-danger"
                onClick={this.deleteManyRole}
              >
                OK
              </button>
              <button
                type="button"
                className="btn grey btn-outline-secondary "
                onClick={this.handleCloseModalMany}
              >
                Cancel
              </button>
            </Modal.Footer>
          </Modal>

          <Header />
          <MenuSideBar />
          {/* Panel search */}

          {/* BEGIN: Content*/}
          <div className="app-content content">
            <div className="content-overlay" />

            <div className="content-wrapper">
              <div className="content-header row">
                <div className="content-header-left col-md-6 col-12 mb-2">
                  <h3 className="content-header-title mb-0">User</h3>
                  <div className="row breadcrumbs-top">
                    <div className="breadcrumb-wrapper col-12">
                      <ol className="breadcrumb">
                        <li className="breadcrumb-item">
                          <Link to="/user">Home</Link>
                        </li>
                        <li className="breadcrumb-item active">User</li>
                      </ol>
                    </div>
                  </div>
                </div>
                <div className="content-header-right text-md-right col-md-6 col-12">
                  <div className="form-group">
                    <Link
                      className="btn-icon btn fonticon-container"
                      type="button"
                      to="/user/create"
                    >
                      <i className="fa fa-plus"></i>
                      <label className="fonticon-classname">Add User</label>
                    </Link>
                    <button
                      className="btn-icon btn fonticon-container"
                      type="button"
                      onClick={() => this.deleteManyModal()}
                    >
                      <i className="fa fa-times"></i>
                      <label className="fonticon-classname">Delete</label>
                    </button>
                    <button
                      className="btn-icon btn fonticon-container"
                      type="button"
                      onClick={this.reloadPage}
                    >
                      <i className="fa fa-refresh"></i>
                      <label className="fonticon-classname">Refresh</label>
                    </button>
                  </div>
                </div>
              </div>
              <div className="content-body">
                {/* Basic Elements start */}
                <section className="basic-elements">
                  <div className="row">
                    <div className="col-md-12">
                      <div className="card">
                        <div className="card-content">
                          <div className="card-body">
                            <div className="row">
                              <div className="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <TextField
                                  id="usernameId"
                                  label="Username"
                                  className="form-control"
                                  color="secondary"
                                  value={this.state.name}
                                  name="username"
                                  onChange={(e) => this.onChangeInput(e)}
                                />
                              </div>
                              <div className="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <TextField
                                  id="fullNameId"
                                  label="Full Name"
                                  className="form-control"
                                  color="secondary"
                                  name="fullName"
                                  value={this.state.fullName}
                                  onChange={(e) => this.onChangeInput(e)}
                                />
                              </div>
                              <div className="col-xl-3 col-lg-6 col-md-12 mb-1">
                                <TextField
                                  id="emailId"
                                  label="Email"
                                  className="form-control"
                                  color="secondary"
                                  name="email"
                                  value={this.state.email}
                                  onChange={(e) => this.onChangeInput(e)}
                                />
                              </div>
                              <div className="col-xl-1 col-lg-1 col-md-1 mb-1">
                                <a
                                  href="#"
                                  className="btn btn-social-icon btn-outline-twitter btn-outline-cyan"
                                  onClick={this.search}
                                >
                                  <span className="fa fa-search"></span>
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                {/* Basic Inputs end */}

                <section id="decimal">
                  <div className="row">
                    <div className="col-12">
                      <div className="card">
                        <div className="card-header">
                          <a href="/#" className="heading-elements-toggle">
                            <i className="fa fa-ellipsis-v font-medium-3" />
                          </a>
                          <div className="heading-elements">
                            <ul className="list-inline mb-0">
                              <li>
                                <a data-action="collapse">
                                  <i className="feather icon-minus" />
                                </a>
                              </li>
                              <li>
                                <a
                                  data-action="reload"
                                  onClick={this.reloadPage}
                                >
                                  <i className="feather icon-rotate-cw" />
                                </a>
                              </li>
                              <li>
                                <a data-action="expand">
                                  <i className="feather icon-maximize" />
                                </a>
                              </li>
                              <li>
                                <a data-action="close">
                                  <i className="feather icon-x" />
                                </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <div className="card-content collapse show">
                          <div className="card-body card-dashboard">
                            <table className="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th style={{ width: "5%" }}>
                                    <Checkbox
                                      checked={this.state.checkAll}
                                      onChange={this.onChangeCheckboxAll}
                                      name="check"
                                      color="primary"
                                      value="checkAll"
                                    />
                                  </th>
                                  <th style={{ width: "5%" }}>
                                    <a
                                      className="tableHeaderLink"
                                      onClick={() => this.sortBy("id")}
                                    >
                                      ID
                                    </a>
                                  </th>
                                  <th>
                                    <a
                                      className="tableHeaderLink"
                                      onClick={() => this.sortBy("name")}
                                    >
                                      Username
                                    </a>
                                  </th>
                                  <th>
                                    <a
                                      className="tableHeaderLink"
                                      onClick={() => this.sortBy("description")}
                                    >
                                      Full Name
                                    </a>
                                  </th>
                                  <th>
                                    <a
                                      className="tableHeaderLink"
                                      onClick={() => this.sortBy("description")}
                                    >
                                      Email
                                    </a>
                                  </th>
                                  <th style={{ width: "15%" }}>Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                {this.state.users.map((value, index) => (
                                  <tr key={index}>
                                    <td>
                                      <Checkbox
                                        checked={value.isChecked}
                                        color="primary"
                                        onChange={() =>
                                          this.onChangeCheckbox(value)
                                        }
                                      />
                                    </td>
                                    <td className="styleTD">
                                      {index +
                                        (this.state.page - 1) *
                                          this.state.size +
                                        1}
                                    </td>
                                    <td className="styleTD">
                                      {value.username}
                                    </td>
                                    <td className="styleTD">
                                      {value.fullName}
                                    </td>
                                    <td> {value.email}</td>
                                    <td>
                                      <Link
                                        className="btn-icon btn warning editButton"
                                        type="button"
                                        to=""
                                        onClick={() => this.editUser(value.id)}
                                      >
                                        <i className="fa fa-eye"></i>
                                      </Link>
                                      <Link
                                        className="btn-icon btn primary editButton"
                                        type="button"
                                        to={`/user/update/${value.id}`}
                                      >
                                        <i className="fa fa-pencil-square"></i>
                                      </Link>
                                      <button
                                        className="btn-icon btn danger deleteButton"
                                        type="button"
                                        onClick={() =>
                                          this.deleteModal(value.id)
                                        }
                                      >
                                        <i className="fa fa-trash"></i>
                                      </button>
                                    </td>
                                  </tr>
                                ))}
                              </tbody>
                            </table>

                            <div className="row">
                              <div className="offset-5 col-2 text-right">
                                <label className="control-label labelPagination">
                                  Show
                                </label>
                              </div>
                              <div className="col-1">
                                <select
                                  name="size"
                                  className="custom-select custom-select-sm form-control form-control-sm"
                                  value={this.state.size}
                                  onChange={this.onChangeSizePage}
                                >
                                  <option value="10">10</option>
                                  <option value="20">20</option>
                                  <option value="30">30</option>
                                </select>
                              </div>
                              <div className="col-4">
                                <Pagination
                                  name="page"
                                  count={this.state.totalPages}
                                  color="primary"
                                  showFirstButton
                                  showLastButton
                                  onChange={this.onChangePage}
                                  page={this.state.page}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
          {/* Modal */}

          <Footer />
          {/* END: Content*/}
        </React.Fragment>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  users: state.user.users,
  totalPages: state.user.totalPages,
  totalElements: state.user.totalElements,
  page: state.user.page,
  errors: state.errors,
});

User.propTypes = {
  users: PropTypes.array.isRequired,
  totalPages: PropTypes.number.isRequired,
  totalElements: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  deleteUser: PropTypes.func.isRequired,
  deleteListUser: PropTypes.func.isRequired,
};
export default connect(mapStateToProps, {
  getUser,
  deleteUser,
  deleteListUser,
})(User);
