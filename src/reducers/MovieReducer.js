import {
  GET_MOVIE_PAGINATION,
  GET_MOVIE_DETAIL,
  GET_ALL_MOVIE,
} from "../action_types/types";

const initialState = {
  allMovie: [],
  movies: [],
  totalPages: 0,
  totalElements: 0,
  page: 0,
  movieDetail: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_MOVIE_PAGINATION:
      return {
        ...state,
        movies: action.payload.movies,
        totalPages: action.payload.totalPages,
        totalElements: action.payload.totalElements,
        page: action.payload.page,
      };
    case GET_ALL_MOVIE:
      return {
        ...state,
        allMovie: action.payload,
      };
    case GET_MOVIE_DETAIL:
      return {
        ...state,
        movieDetail: action.payload,
      };
    default:
      return state;
  }
}
