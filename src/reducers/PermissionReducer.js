import {
  GET_PERMISSION_DETAIL,
  DELETE_PERMISSION,
  GET_PERMISSION_PAGINATION,
  GET_PERMISSION_ALL,
} from "../action_types/types";

const initialState = {
  allPermission:[],
  permission: [],
  totalPages: 0,
  totalElements: 0,
  page: 0,
  permissionDetail: {},
  deleteSuccess: "",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_PERMISSION_ALL:
      return {
        ...state,
        allPermission: action.payload
      };
    case GET_PERMISSION_PAGINATION:
      return {
        ...state,
        permission: action.payload.permission,
        totalPages: action.payload.totalPages,
        totalElements: action.payload.totalElements,
        page: action.payload.page,
      };
    case GET_PERMISSION_DETAIL:
      return {
        ...state,
        permissionDetail: action.payload,
      };
      
    case DELETE_PERMISSION:
      return {
        ...state,
        deleteSuccess: action.payload,
      };
    default:
      return state;
  }
}
